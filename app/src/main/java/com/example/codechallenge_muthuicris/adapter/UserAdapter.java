package com.example.codechallenge_muthuicris.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.codechallenge_muthuicris.R;
import com.example.codechallenge_muthuicris.activity.UserActivity;
import com.example.codechallenge_muthuicris.activity.UserDetailsActivity;
import com.example.codechallenge_muthuicris.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyViewHolder> implements Filterable {
    private List<User> userList;
    private List<User> userFiltered;
    private Context context;

    public void setUser(Context context,final List<User> userList){
        this.context = context;
        if(this.userList == null){
            this.userList = userList;
            this.userFiltered = userList;
            notifyItemChanged(0, userFiltered.size());
        } else {
            final DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return UserAdapter.this.userList.size();
                }

                @Override
                public int getNewListSize() {
                    return userList.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return UserAdapter.this.userList.get(oldItemPosition).getName() == userList.get(newItemPosition).getName();
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {

                    User newUser = UserAdapter.this.userList.get(oldItemPosition);

                    User oldUser = userList.get(newItemPosition);

                    return newUser.getName() == oldUser.getName() ;
                }
            });
            this.userList = userList;
            this.userFiltered = userList;
            result.dispatchUpdatesTo(this);
        }
    }

    @Override
    public UserAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserAdapter.MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.name.setText(userFiltered.get(position).getName());
        holder.email.setText(userFiltered.get(position).getEmail());
        holder.phone.setText(userFiltered.get(position).getAddress().getSuite());
        holder.city.setText(userFiltered.get(position).getAddress().getStreet());
        String letter =  String.valueOf(userFiltered.get(position).getName().charAt(0));
        holder.imageLetter.setText(letter);
        //set Rando colors of the users profile
        int[] androidColors = context.getResources().getIntArray(R.array.mdcolor_random);
        int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
        holder.image.setBackgroundColor(randomAndroidColor);
//        holder.relativeLayoutImage.setBackgroundColor(randomAndroidColor);

        holder.linearLayoutParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, UserDetailsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                intent.putExtra("user",userFiltered.get(position));
                context.startActivity(intent);
            }
        });



    }

    @Override
    public int getItemCount() {

        if(userList != null){
            return userFiltered.size();
        } else {
            return 0;
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    userFiltered = userList;
                } else {
                    List<User> filteredList = new ArrayList<>();
                    for (User movie : userList) {
                        if (movie.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(movie);
                        }
                    }
                    userFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = userFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                userFiltered = (ArrayList<User>) filterResults.values;

                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        ImageView image;
        TextView email;
        TextView city;
        TextView phone;
        TextView imageLetter;
        LinearLayout linearLayoutParent;
        RelativeLayout relativeLayoutImage;


        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            email = (TextView) view.findViewById(R.id.email);
            city = (TextView) view.findViewById(R.id.city);
            phone = (TextView) view.findViewById(R.id.phone);
            phone = (TextView) view.findViewById(R.id.phone);
            imageLetter = (TextView) view.findViewById(R.id.image_letter);
            image = (ImageView)view.findViewById(R.id.profile);
            linearLayoutParent = (LinearLayout)view.findViewById(R.id.lyt_parent);
            relativeLayoutImage = (RelativeLayout) view.findViewById(R.id.lyt_image);
        }
    }
}
