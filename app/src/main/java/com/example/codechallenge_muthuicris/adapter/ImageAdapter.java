package com.example.codechallenge_muthuicris.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.codechallenge_muthuicris.R;
import com.example.codechallenge_muthuicris.model.ImageDto;

import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {
    private List<ImageDto> imageList;
    private Context mContext;

    public ImageAdapter(List<ImageDto> imageDtoList, Context context) {
        imageList = imageDtoList;
        mContext = context;
        for (int i=0;i < imageList.size();i++){
            Log.d("Image List",imageList.get(i).getImage().toString());
        }

    }

    public ImageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.image_item,
                parent, false);
        ViewHolder imageViewHolder = new ViewHolder(view);
        return imageViewHolder;

    }

    public void onBindViewHolder(@NonNull ImageAdapter.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        Log.d("images",imageList.get(position).getImage().toString());
        //holder.linearLayout.setBackground(ob);
        holder.imageView.setImageBitmap(imageList.get(position).getImage());

    }

    public int getItemCount() {
        return imageList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView,deleteIcon;
        public TextView textView;
        public LinearLayout linearLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            //linearLayout= (LinearLayout) itemView.findViewById(R.id.image_display);
            imageView = (ImageView) itemView.findViewById(R.id.sample_image);
            deleteIcon = (ImageView) itemView.findViewById(R.id.delete);
            deleteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeAt(getPosition());
                }
            });

        }


    }
    public void removeAt(int position) {
        imageList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, imageList.size());
    }
}
