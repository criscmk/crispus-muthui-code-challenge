package com.example.codechallenge_muthuicris.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;

public class ConnectivityReceiver extends BroadcastReceiver {
    public static ConnectivityReceiverListener connectivityReceiverListener;

    public ConnectivityReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
      NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
      boolean isConnected = activeNetwork!=null && activeNetwork.isConnectedOrConnecting();
      if(connectivityReceiverListener!=null)
      {
          connectivityReceiverListener.OnNetworkConnectionChanged(isConnected);

      }

    }
    //check manually

    //create an interface
    public interface ConnectivityReceiverListener{
        Void OnNetworkConnectionChanged(boolean isConnected);
    }

}
