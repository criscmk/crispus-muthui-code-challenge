package com.example.codechallenge_muthuicris.model;

import android.graphics.Bitmap;

public class ImageDto {
    Bitmap image;
    public ImageDto(Bitmap image) {
        this.image = image;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }


}
