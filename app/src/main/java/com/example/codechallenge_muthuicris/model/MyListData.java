package com.example.codechallenge_muthuicris.model;

import android.graphics.Bitmap;

public class MyListData {

    private Bitmap imgId;

    public MyListData( Bitmap imgId) {

        this.imgId = imgId;
    }




    public Bitmap getImgId() {
        return imgId;
    }

    public void setImgId(Bitmap imgId) {
        this.imgId = imgId;
    }
}