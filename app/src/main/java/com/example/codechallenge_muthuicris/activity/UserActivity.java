package com.example.codechallenge_muthuicris.activity;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidstudy.networkmanager.Monitor;
import com.androidstudy.networkmanager.Tovuti;
import com.example.codechallenge_muthuicris.R;
import com.example.codechallenge_muthuicris.RetrofitClient.RetrofitClient;
import com.example.codechallenge_muthuicris.adapter.UserAdapter;
import com.example.codechallenge_muthuicris.model.User;
import com.example.codechallenge_muthuicris.utils.Tools;
import com.example.codechallenge_muthuicris.widget.LineItemDecoration;

import java.io.Serializable;
import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserActivity extends AppCompatActivity {
    private SearchView searchView;
    private RecyclerView recyclerView;
    private UserAdapter userAdapter;
    private List<User> userList;
    private ProgressBar progressBar;
    private View parent_view;
    private Toolbar toolbar;
    private FloatingActionButton photo, map;
    private Object user;
    private Dialog dialog;
    private boolean connectionStatus,usersPresent=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new LineItemDecoration(this, LinearLayout.VERTICAL));
        userAdapter = new UserAdapter();
        recyclerView.setAdapter(userAdapter);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        photo = (FloatingActionButton) findViewById(R.id.camera);
        map = (FloatingActionButton) findViewById(R.id.map);
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserActivity.this, GallaryActivity.class);
                startActivity(intent);
            }
        });
        initToolbar();
        checkInternetConnection();

    }

    //initialize toolbar
    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Users");
        Tools.setSystemBarColor(this, R.color.blue_600);

    }

    //get users from the api
    private void getUsers() {
       progressBar.setVisibility(View.VISIBLE);
            Call<List<User>> call = RetrofitClient
                    .getInstance()
                    .getApi()
                    .getUsers();
            call.enqueue(new Callback<List<User>>() {
                @Override
                public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                    progressBar.setVisibility(View.GONE);

                    Log.d("response main", response.toString());

                    if (response.isSuccessful()) {
                        usersPresent = true;
                        userList = response.body();
                        user = response.body();
                        userAdapter.setUser(getApplicationContext(), userList);
                        map.setVisibility(View.VISIBLE);
                        map.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(UserActivity.this, MapsActivity.class);
                                intent.putExtra("user", (Serializable) user);
                                startActivity(intent);
                            }
                        });

                    }
                }

                @Override
                public void onFailure(Call<List<User>> call, Throwable t) {
                    Log.d("error", t.toString());
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                    progressBar.setVisibility(View.GONE);


                }


            });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_setting, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                userAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                userAdapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }
//check internet connection before invoking users api call
    private void checkInternetConnection() {
        Tovuti.from(this).monitor(new Monitor.ConnectivityListener() {
            @Override
            public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                connectionStatus = isConnected;
                if(connectionStatus && !usersPresent)
                {
                    getUsers();}
                if(!connectionStatus && !usersPresent)
                {
                   NoInternetDialog();
                }

            }
        });


    }
//display no internet connection dialog
    public void NoInternetDialog() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.no_internet_connection_layout);
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        ((AppCompatButton) dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(), ((AppCompatButton) v).getText().toString() + " Clicked", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);

    }
}