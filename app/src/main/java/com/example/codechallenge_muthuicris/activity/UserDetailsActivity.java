package com.example.codechallenge_muthuicris.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.codechallenge_muthuicris.R;
import com.example.codechallenge_muthuicris.model.User;
import com.example.codechallenge_muthuicris.utils.Tools;

import org.json.JSONException;
import org.json.JSONObject;

public class UserDetailsActivity extends AppCompatActivity {
    private User user;
    private Toolbar toolbar;
    private String name,username,phone,website,email,suite,street,city,zipCode;
    private String companyName,catchPhrase,bs;
    private TextView textViewEmail,textViewUsername,textViewPhone,textViewWebsite;
    private TextView textViewStreet,textViewSuite,textViewCity,textViewZipCode;
    private TextView textViewCompanyName,textViewCatchPhrase,textViewBs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        Intent intent = getIntent();
        //get value of the user from the intent
        if(getIntent().getExtras() != null) {
            User user = (User) getIntent().getSerializableExtra("user");
            name = user.getName();
            username = user.getUsername();
            website = user.getWebsite();
            phone = user.getPhone();
            email = user.getEmail();
            suite = user.getAddress().getSuite();
            street = user.getAddress().getStreet();
            city = user.getAddress().getCity();
            zipCode = user.getAddress().getZipcode();
            companyName = user.getCompany().getName();
            catchPhrase = user.getCompany().getCatchPhrase();
            bs = user.getCompany().getBs();

        }

        initComponent();
        setValues();
        initToolbar();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    private void initComponent()
    {
        //initialize components
        textViewUsername = (TextView)findViewById(R.id.username);
        textViewPhone = (TextView)findViewById(R.id.phone);
        textViewWebsite = (TextView)findViewById(R.id.website);
        textViewEmail= (TextView)findViewById(R.id.email);
        textViewStreet = (TextView)findViewById(R.id.street);
        textViewSuite = (TextView)findViewById(R.id.suit);
        textViewCity = (TextView)findViewById(R.id.city);
        textViewZipCode = (TextView)findViewById(R.id.zipcode);
        textViewCompanyName = (TextView)findViewById(R.id.company_name);
        textViewCatchPhrase = (TextView)findViewById(R.id.catch_phrase);
        textViewBs = (TextView)findViewById(R.id.bs);

    }
    private void setValues()
    {
        //set values
        textViewUsername.setText(username);
        textViewPhone.setText(phone);
        textViewEmail.setText(email);
        textViewWebsite.setText(website);
        textViewStreet.setText(street);
        textViewSuite.setText(suite);
        textViewCity.setText(city);
        textViewZipCode.setText(zipCode);
        textViewCompanyName.setText(companyName);
        textViewCatchPhrase.setText(catchPhrase);
        textViewBs.setText(bs);
    }
    private void initToolbar() {
        //initialize toolbar
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(name);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Tools.setSystemBarColor(this, R.color.blue_600);
    }
}