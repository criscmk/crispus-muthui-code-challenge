package com.example.codechallenge_muthuicris.activity;

import android.Manifest;
import android.app.ActionBar;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;


import com.example.codechallenge_muthuicris.R;
import com.example.codechallenge_muthuicris.adapter.ImageAdapter;
import com.example.codechallenge_muthuicris.model.ImageDto;
import com.example.codechallenge_muthuicris.model.MyListData;
import com.example.codechallenge_muthuicris.utils.Tools;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;

public class GallaryActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private ImageAdapter imageAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Bitmap> arrayList = new ArrayList();
    private List<ImageDto> imageDtoList= new ArrayList<>();
    private Toolbar toolbar;
    AlertDialog.Builder builder;
    private ImageDto imageDto;
    ImageAdapter mAdapter;
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallary);
        recyclerView = (RecyclerView) findViewById(R.id.image_recyclerview);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.choose_image);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage(GallaryActivity.this);
            }
        });
      checkStoragePermision();
      initToolbar();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    //initialize toolbar
    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("List Images");
        Tools.setSystemBarColor(this, R.color.blue_600);

    }
    private void selectImage(Context context) {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        builder= new AlertDialog.Builder(context);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 0);

                } else if (options[item].equals("Choose from Gallery")) {
                    String[] galleryPermissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    if (EasyPermissions.hasPermissions(GallaryActivity.this, galleryPermissions)) {

                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, 1);//one can be replaced with any action code
                    } else {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, 1);//one can be replaced with any action code
                        EasyPermissions.requestPermissions(GallaryActivity.this, "Access for storage",
                                101, galleryPermissions);

                    }


                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK && data != null) {
                        Bitmap selectedImage = (Bitmap) data.getExtras().get("data");
                        Log.d("image1", selectedImage.toString());
                        //imageView.setImageBitmap(selectedImage);
                        displayImage(selectedImage);
                    }

                    break;
                case 1:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri selectedImage = data.getData();

                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        if (selectedImage != null) {
                            Cursor cursor = getContentResolver().query(selectedImage,
                                    filePathColumn, null, null, null);
                            if (cursor != null) {
                                cursor.moveToFirst();
                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                String picturePath = cursor.getString(columnIndex);
                                //imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                                Bitmap imageSelected = BitmapFactory.decodeFile(picturePath);
                                displayImage(imageSelected);
                                cursor.close();
                            }
                        }

                    }
                    break;
            }
        }
    }

    public void displayImage(Bitmap imageSelected)
    {
        imageDto = new ImageDto(imageSelected);
        arrayList.add(imageSelected);
        imageDto = new ImageDto(imageSelected);
        imageDtoList.add(imageDto);
        imageAdapter = new ImageAdapter(imageDtoList,this);
//                                Log.d("Listssssss",myListData.toString());
        // specify an adapter and pass in our data model list
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new ImageAdapter(imageDtoList,this);
        recyclerView.setAdapter(mAdapter);

    }
    public void checkStoragePermision()
    {
        String[] galleryPermissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(this, galleryPermissions)) {

        } else {
            EasyPermissions.requestPermissions(this, "Access for storage",
                    101, galleryPermissions);
        }
    }
}