
package com.example.codechallenge_muthuicris.api;
import com.example.codechallenge_muthuicris.model.User;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

   @GET("users")
    Call<List<User>> getUsers();



}
