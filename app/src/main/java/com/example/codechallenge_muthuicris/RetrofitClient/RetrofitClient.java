package com.example.codechallenge_muthuicris.RetrofitClient;

import com.example.codechallenge_muthuicris.api.ApiInterface;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static final String BaseUrl = "https://jsonplaceholder.typicode.com/";
    private static RetrofitClient mInstance;
    private Retrofit retrofit, retrofit2;

    private RetrofitClient(){
        retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }

    public static synchronized RetrofitClient getInstance(){
        if (mInstance == null)
        {
            mInstance = new RetrofitClient();
        }

        return mInstance;
    }

    public ApiInterface getApi()
    {
        return retrofit.create(ApiInterface.class);
    }



}
